from dataclasses import asdict, dataclass
from datetime import datetime


@dataclass
class DocumentTypes:
    uuid: str
    label: str
    value: str

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass
class StudentSummary:
    uuid: str  # UUID
    first_name: str
    last_name: str
    document_number: str
    document_type: DocumentTypes  # puede llevar uuid o id de document_type
    created_at: datetime
    modified_at: datetime

    def to_dict(self) -> dict:
        return asdict(self)

    @property
    def get_document_type(self):
        return self.document_type.label
